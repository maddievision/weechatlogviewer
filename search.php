<?php
require('lib/lib.php');
$buffers = GetWeechatBuffers();
$sk = $_GET['sk'];
$bk = $_GET['bk'];
if (isset($_GET['q'])) {
	$sq = $_GET['q'];
}
else $sq = "";
if (isset($_GET['c'])) {
	$cs = ($_GET['c']=='on')?1:0;
}
else {
	$cs = 0;
}

$title .= " - $sk/$bk Search";
require('view/header.php');
$bf = $buffers[$sk][$bk];
$q = http_build_query(array(
        sk => $sk,
        bk => $bk));
$surl = "search.php";
$hurl = "buffer.php";
if ($sq != "") {


}
echo "<h1>$sk/$bk</h1>";
?>
          <div class="container">
          <ul class="tabs">
          <li><a href="<?php echo $hurl.'?'.$q; ?>">History</a></li>
          <li class="active"><a href="#">Search</a></li>
          </div>
          <h2>Search</h2>
<?php
if ($sq != "") {
?>
<div id="results" class="container">
<p>Showing results for <code><?php echo htmlentities($sq); ?></code></p>       
<?php
if ($cs == 1) {
?>
    <div class="alert-message block-message warning">
    <p>This is a case sensitive search.</p>
    </div>


<?php
}
?>
<pre><?php grep_file($bf, $sq, $cs); ?></pre>
</div>
<?php
}
?>

          <form action="<?php echo $surl; ?>" class="form-stacked">
              <fieldset>
                  <div class="clearfix">
                      <label for="q">Search for:</label>
                      <div class="input">
                          <input class="xlarge" id="q" name="q" size="30" type="text" />
                      </div>
                  </div><!-- /clearfix -->
                  <div class="clearfix">
                    <label id="optionsCheckboxes">Search options</label>
                    <div class="input">
                      <ul class="inputs-list">
                        <li>
                          <label>
                            <input type="checkbox" id="c" name="c" value="on" />
                            <span>Case sensitive search</span>
                          </label>
                        </li>
                      </ul>
                    </div>
                  </div><!-- /clearfix -->
              </fieldset>
              <div class="actions">
                <input type="hidden" name="sk" id="sk" value="<?php echo $sk; ?>" />
                <input type="hidden" name="bk" id="bk" value="<?php echo $bk; ?>" />
                <button type="submit" class="btn primary">Search</button>
              </div>
          </form>

<?php
require('view/footer.php');
?>
