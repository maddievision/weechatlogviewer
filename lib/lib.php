<?php
require("conf/config.php");
$title = "$sitename - ";
function hfix($str) {
	return str_replace("#", "%23", $str);
}
function grep_file($bf, $sq, $cs) {
	$x = "";
	if ($cs != 1) {
		$cq = " -i";
	}
	else $cq = "";
	passthru("grep$cq ".escapeshellarg($sq)." ".escapeshellarg($bf),$x);
	return $x;  
}

function read_file($file, $lines) {
    //global $fsize;
    $handle = fopen($file, "r");
    $linecounter = $lines;
    $pos = -2;
    $beginning = false;
    $text = array();
    while ($linecounter > 0) {
        $t = " ";
        while ($t != "\n") {
            if(fseek($handle, $pos, SEEK_END) == -1) {
                $beginning = true; 
                break; 
            }
            $t = fgetc($handle);
            $pos --;
        }
        $linecounter --;
        if ($beginning) {
            rewind($handle);
        }
        $text[$lines-$linecounter-1] = fgets($handle);
        if ($beginning) break;
    }
    fclose ($handle);
    return array_reverse($text);
}

function GetWeechatBuffers() {
global $logdir;
$rd = array();
$files = array();
if ($handle = opendir($logdir)) {
        $count = 0;
    while (false !== ($file = readdir($handle))) {
        $files[$count++] = $file;
    }
    closedir($handle);
}
        $servers = array();
        $scount = 0;
        $bcount = array();
        $buffers = array();
        $bfilename = array();
foreach ($files as $k => $v) {
        preg_match_all("/irc\.([^.]+)\.(.+)\.weechatlog/", $v, $matches, PREG_SET_ORDER);
        foreach($matches as $mt) {
                $sname = $mt[1];
                $bname = $mt[2];
                if (!isset($buffers[$sname])) {
                        $buffers[$sname] = array();
                        $bcount[$sname] = 0;
                        $servers[$scount++] = $sname;
                        $bfilename[$sname] = array();
                }
                $buffers[$sname][$bcount[$sname]++] = $bname;
                $bfilename[$sname][$bname] = $logdir.'/'.$v;
        }
}
        sort($servers);
        foreach($servers as $sk) {
                sort($buffers[$sk]);
                $rd[$sk] = array();
                foreach($buffers[$sk] as $bn) {
                        $rd[$sk][$bn] = $bfilename[$sk][$bn];
                }
        }
        return $rd;
}
?>
