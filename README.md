Under heavy construction!

## Disclaimer

This was put together in the space of one hour for a very targeted purpose, and is currently very messy and hacky.

Should you be crazy enough to decide to use this be sure you put it behind good security (do not make it public)!

## Instructions

1. Edit `conf/config.sample.php` (there's only one thing you need to change) and save it as `conf/config.php`.
2. You are good to go!
